import React, {useLayoutEffect} from "react";
import DeleteModal from "@/components/DeleteModal";

import {useDispatch} from "react-redux";
import {crud} from "@/redux/crud/actions";


import AdminDataTable from "./AdminDataTable";
import ListingGuide from "@/layout/CrudLayout/ListingGuide";


function ListingGuideModule({config, createForm, updateForm}) {
    const dispatch = useDispatch();

    useLayoutEffect(() => {
        dispatch(crud.resetState());
    }, []);

    return (<ListingGuide
        config={config}
    >
        <AdminDataTable config={config}/>
        <DeleteModal config={config}/>
    </ListingGuide>);
}

export default ListingGuideModule;