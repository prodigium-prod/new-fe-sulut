import React, {useState} from "react";
import {Link} from "react-router-dom";
import {Layout, Menu} from "antd";
import {CustomerServiceOutlined, DashboardOutlined, FileSyncOutlined, SettingOutlined,} from "@ant-design/icons";

const {Sider} = Layout;
const {SubMenu} = Menu;

function Navigation() {
    const [selectedPage, setSelectedPage] = useState(null); // State to store the selected page

    const [collapsed, setCollapsed] = useState(false);

    const onCollapse = () => {
        setCollapsed(!collapsed);
    };

    const handleMenuClick = (page) => {
        setSelectedPage(page); // Update the selected page state when a menu item is clicked
    };

    return (<>
        <Sider
            collapsible
            collapsed={collapsed}
            onCollapse={onCollapse}
            style={{
                zIndex: 1000,
            }}
        >
            <div className="logo"/>
            <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
                <Menu.Item
                    key="1"
                    icon={<DashboardOutlined/>}
                    onClick={() => handleMenuClick("home")} // Update selectedPage when Home Page is clicked
                >
                    <Link to="/"/>
                    Home Page
                </Menu.Item>
                <SubMenu key="sub1" icon={<FileSyncOutlined/>} title="Accommodation">
                    <Menu.Item
                        key="2"
                        onClick={() => handleMenuClick("hotel")} // Update selectedPage when Hotel is clicked
                    >
                        <Link to="/hotel"/>
                        Accommodation
                    </Menu.Item>
                    <Menu.Item
                        key="3"
                        onClick={() => handleMenuClick("hotelDetail")} // Update selectedPage when Hotel Detail is clicked
                    >
                        <Link to="/hotel-detail"/>
                        Detail
                    </Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<FileSyncOutlined/>} title="Dining">
                    <Menu.Item
                        key="4"
                        onClick={() => handleMenuClick("fnb")} // Update selectedPage when Hotel is clicked
                    >
                        <Link to="/fnb"/>
                        Dining
                    </Menu.Item>
                    <Menu.Item
                        key="5"
                        onClick={() => handleMenuClick("menu")} // Update selectedPage when Hotel Detail is clicked
                    >
                        <Link to="/menu"/>
                        Menu
                    </Menu.Item>
                </SubMenu>
                <Menu.Item
                    key="6"
                    icon={<CustomerServiceOutlined/>}
                    onClick={() => handleMenuClick("publicservices")} // Update selectedPage when Restaurant is clicked
                >
                    <Link to="/publicservices"/>
                    General Public
                </Menu.Item>
                <Menu.Item
                    key="32"
                    icon={<SettingOutlined/>}
                    onClick={() => handleMenuClick("admin")} // Update selectedPage when Settings is clicked
                >
                    <Link to="/admin"/>
                    Settings
                </Menu.Item>
            </Menu>
        </Sider>
    </>);
}

export default Navigation;