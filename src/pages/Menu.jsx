import React from "react";

import CrudModule from "@/modules/CrudModule";
import MenuForm from "@/forms/MenuForm";

function Menu() {
    const entity = "menu";
    const searchConfig = {
        displayLabels: ["Name"],
        searchFields: "name",
        outputValue: "name",
    }

    const panelTitle = "Menu Detail";
    const dataTableTitle = "Tenant FnB & Gift Shop";
    const entityDisplayLabels = ["name"];

    const readColumns = [
        {
          title: "Name",
          dataIndex: "name",
        },
        {
          title: "Description",
          dataIndex: "description",
        },
        {
          title: "Price",
          dataIndex: "price",
        },
        {
          title: "Availability",
          dataIndex: "availability",
        },
        {
          title: "Status",
          dataIndex: "status",
        },
        {
          title: "Images",
          dataIndex: "imageSrc",
            render: (imageSrc) => (
                <>
                    {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                        imageSrc.map((image, index) => (
                            <img
                                key={index}
                                src={image.url}
                                alt={`Hotel Image ${index}`}
                                width="100"
                                style={{ marginRight: 10 }}
                            />
                        ))
                    ) : (
                        <span>No images available</span>
                    )}
                </>
            ),
        },
      ];
      const dataTableColumns = [
         {
            title: "Name",
            dataIndex: "name",
          },
          {
            title: "Description",
            dataIndex: "description",
          },
          {
            title: "Price",
            dataIndex: "price",
          },
          {
            title: "Availability",
            dataIndex: "availability",
          },
          {
            title: "Status",
            dataIndex: "status",
          },
          {
            title: "Images",
            dataIndex: "imageSrc",
              render: (imageSrc) => (
                  <>
                      {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                          imageSrc.map((image, index) => (
                              <img
                                  key={index}
                                  src={image.url}
                                  alt={`Hotel Image ${index}`}
                                  width="100"
                                  style={{ marginRight: 10 }}
                              />
                          ))
                      ) : (
                          <span>No images available</span>
                      )}
                  </>
              ),
          },
      ];

      const ADD_NEW_ENTITY = "Add New Menu";
      const DATATABLE_TITLE = "Menu FnB & Gift Shop List";
      const ENTITY_NAME = "name";
      const CREATE_ENTITY = "Create Menu FnB & Gift Shop";
      const UPDATE_ENTITY = "Update Menu FnB & Gift Shop";
      const config = {
          entity,
          panelTitle,
          dataTableTitle,
          ENTITY_NAME,
          CREATE_ENTITY,
          ADD_NEW_ENTITY,
          UPDATE_ENTITY,
          DATATABLE_TITLE,
          readColumns,
          dataTableColumns,
          searchConfig,
          entityDisplayLabels,
      };
      return (
          <CrudModule
          createForm={<MenuForm />}
          updateForm={<MenuForm isUpdateForm={true} />}
          config={config}
          />
      );
}
export default Menu;