import React from "react";

import CrudModule from "@/modules/CrudModule";
import FnbForm from "@/forms/FnbForm";

function Fnb() {
    const entity = "fnb";
    const searchConfig = {
        displayLabels: ["Name"], searchFields: "name", outputValue: "name",
    }

    const panelTitle = "Dining";
    const dataTableTitle = "Dining";
    const entityDisplayLabels = ["name"];

    const readColumns = [{
        title: "Name", dataIndex: "name",
    }, {
        title: "Description", dataIndex: "description",
    }, {
        title: "Address", dataIndex: "address",
    }, {
        title: "Phone", dataIndex: "phone",
    }, {
        title: "Open Hour", dataIndex: "open_hour",
    }, {
        title: "Close Hour", dataIndex: "close_hour",
    }, {
        title: "Image",
        dataIndex: "imageSrc",
        render: (imageSrc) => <img src={imageSrc[0].url} alt="Hotel Image" width="100"/>,
    },];
    const dataTableColumns = [{
        title: "Name", dataIndex: "name",
    },
        {
        title: "Description", dataIndex: "description",
    },{
            title: "Category", dataIndex: "category",
        }
    , {
        title: "Address", dataIndex: "address",
    }, {
        title: "Phone", dataIndex: "phone",
    }, {
        title: "Open Hour", dataIndex: "open_hour",
    }, {
        title: "Close Hour", dataIndex: "close_hour",
    }, {
        title: "Image",
        dataIndex: "imageSrc",
        render: (imageSrc) => (Array.isArray(imageSrc) && imageSrc.length > 0 && imageSrc[0].url ? (
                <img src={imageSrc[0].url} alt="Hotel Image" width="100"/>) : (<span>No image available</span>)),
    }];

    const ADD_NEW_ENTITY = "Add New Dining site";
    const DATATABLE_TITLE = "Dining List";
    const ENTITY_NAME = "name";
    const CREATE_ENTITY = "Create Dining";
    const UPDATE_ENTITY = "Update Dining";
    const config = {
        entity,
        panelTitle,
        dataTableTitle,
        ENTITY_NAME,
        CREATE_ENTITY,
        ADD_NEW_ENTITY,
        UPDATE_ENTITY,
        DATATABLE_TITLE,
        readColumns,
        dataTableColumns,
        searchConfig,
        entityDisplayLabels,
    };
    return (<CrudModule
            createForm={<FnbForm/>}
            updateForm={<FnbForm isUpdateForm={true}/>}
            config={config}
        />);
}

export default Fnb;