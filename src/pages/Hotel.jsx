import React from "react";

import CrudModule from "@/modules/CrudModule";
import HotelForm from "@/forms/HotelForm";

function Hotel() {
    const entity = "hotel";
    const searchConfig = {
        displayLabels: ["Title"],
        searchFields: "title",
        outputValue: "title",
    }

    const panelTitle = "Accommodation Panel";
    const dataTableTitle = "Accommodation List";
    const entityDisplayLabels = ["title"];

    const readColumns = [
        {
          title: "Title",
          dataIndex: "title",
        },
        {
          title: "Address",
          dataIndex: "address",
        },
        {
          title: "Phone",
          dataIndex: "phone",
        },
        {
          title: "Star",
          dataIndex: "star",
        },
        {
          title: "Images",
          dataIndex: "imageSrc",
            render: (imageSrc) => (
                <>
                    {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                        imageSrc.map((image, index) => (
                            <img
                                key={index}
                                src={image.url}
                                alt={`Hotel Image ${index}`}
                                width="100"
                                style={{ marginRight: 10 }}
                            />
                        ))
                    ) : (
                        <span>No images available</span>
                    )}
                </>
            ),
        },
      ];
      const dataTableColumns = [
        {
          title: "Title",
          dataIndex: "title",
        },
        {
          title: "Address",
          dataIndex: "address",
        },
        {
          title: "Phone",
          dataIndex: "phone",
        },
        {
          title: "Star",
          dataIndex: "star",
        },
        {
          title: "Images",
          dataIndex: "imageSrc",
            render: (imageSrc) => (
                <>
                    {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                        imageSrc.map((image, index) => (
                            <img
                                key={index}
                                src={image.url}
                                alt={`Hotel Image ${index}`}
                                width="100"
                                style={{ marginRight: 10 }}
                            />
                        ))
                    ) : (
                        <span>No images available</span>
                    )}
                </>
            ),
        },
      ];

      const ADD_NEW_ENTITY = "Add New Accommodation";
      const DATATABLE_TITLE = "Accommodation List";
      const ENTITY_NAME = "hotel";
      const CREATE_ENTITY = "Create Accommodation";
      const UPDATE_ENTITY = "Update Accommodation";
      const config = {
          entity,
          panelTitle,
          dataTableTitle,
          ENTITY_NAME,
          CREATE_ENTITY,
          ADD_NEW_ENTITY,
          UPDATE_ENTITY,
          DATATABLE_TITLE,
          readColumns,
          dataTableColumns,
          searchConfig,
          entityDisplayLabels,
      };
      return (
          <CrudModule
          createForm={<HotelForm />}
          updateForm={<HotelForm isUpdateForm={true} />}
          config={config}
          />
      );
}
export default Hotel;