import React from "react";

import CrudModule from "@/modules/CrudModule";
import HotelDetailForm from "@/forms/HotelDetailForm";

function HotelDetail() {
    const entity = "hotel-detail";
    const searchConfig = {
        displayLabels: ["Title"],
        searchFields: "hotel",
        outputValue: "hotel",
    }

    const panelTitle = "Accommodation Detail Panel";
    const dataTableTitle = "Accommodation Detail List";
    const entityDisplayLabels = ["hotel-detail"];

    const readColumns = [ 
        {
          title: "Hotel",
          dataIndex: "hotel",
        },
        {
          title: "Room",
          dataIndex: "name",
        },
        {
          title: "Price",
          dataIndex: "price",
        },
        {
          title: "Available",
          dataIndex: "available",
        },
        {
          title: "Images",
          dataIndex: "imageSrc",
            render: (imageSrc) => (
                <>
                    {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                        imageSrc.map((image, index) => (
                            <img
                                key={index}
                                src={image.url}
                                alt={`Hotel Image ${index}`}
                                width="100"
                                style={{ marginRight: 10 }}
                            />
                        ))
                    ) : (
                        <span>No images available</span>
                    )}
                </>
            ),
        },
      ];
      const dataTableColumns = [
        {
            title: "Hotel Name",
            dataIndex: "hotel",
          },
          {
            title: "Room",
            dataIndex: "name",
          },
          {
          title: "Price",
          dataIndex: "price",
          },
          {
            title: "Available",
            dataIndex: "available",
          },
          {
            title: "Images",
            dataIndex: "imageSrc",
              render: (imageSrc) => (
                  <>
                      {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                          imageSrc.map((image, index) => (
                              <img
                                  key={index}
                                  src={image.url}
                                  alt={`Hotel Image ${index}`}
                                  width="100"
                                  style={{ marginRight: 10 }}
                              />
                          ))
                      ) : (
                          <span>No images available</span>
                      )}
                  </>
              ),
          },
      ];

      const ADD_NEW_ENTITY = "Add Accommodation Detail";
      const DATATABLE_TITLE = "Accommodation Detail List";
      const ENTITY_NAME = "hotel-detail";
      const CREATE_ENTITY = "Create Accommodation Detail";
      const UPDATE_ENTITY = "Update Accommodation Detail";
      const config = {
          entity,
          panelTitle,
          dataTableTitle,
          ENTITY_NAME,
          CREATE_ENTITY,
          ADD_NEW_ENTITY,
          UPDATE_ENTITY,
          DATATABLE_TITLE,
          readColumns,
          dataTableColumns,
          searchConfig,
          entityDisplayLabels,
      };
      return (
          <CrudModule
          createForm={<HotelDetailForm />}
          updateForm={<HotelDetailForm isUpdateForm={true} />}
          config={config}
          />
      );
}
export default HotelDetail;