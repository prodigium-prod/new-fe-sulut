import React, {useState} from "react";

import AdminForm from "@/forms/AdminForm";
import ListingGuideModule from "@/modules/AdminCrudModule/ListingGuideModule";
import {Button, Image, message, Modal} from "antd";
import {CheckCircleOutlined, CloseCircleOutlined} from "@ant-design/icons";
import axios from "axios";

export default function Admin() {
    const entity = "admin";
    const searchConfig = {
        displayLabels: ["name", "email"], searchFields: "email,name", outputValue: "_id",
    };

    const [showComponent, setShowComponent] = useState('show');

    const panelTitle = "Guide Panel";
    const dataTableTitle = "Guide Lists";
    const entityDisplayLabels = ["email"];

    const [isConfirmationModalVisible, setIsConfirmationModalVisible] = useState(false);

    // Function to toggle the modal
    const toggleConfirmationModal = () => {
        setIsConfirmationModalVisible(!isConfirmationModalVisible);
    };

    const updateConfirmation = async (record) => {
        try {
            const response = await axios.patch(`http://localhost:8888/api/client/activated/${record.id}`);

            if (response.status === 200) {
                toggleConfirmationModal()
                message.success(`${response.data.message}`)
                // Update the data in the state if the API call is successful
            } else {
                console.error('API request failed');
            }
        } catch (error) {
            console.error('API request error:', error);
        }
    };

    const readColumns = [{title: "Name", dataIndex: "name"}, {
        title: "Email",
        dataIndex: "email"
    }, {title: "confirmation", dataIndex: "confirmation"},];

    const dataTableColumns = [
        {title: "Name", dataIndex: "name"}, {title: "Email", dataIndex: "email"},
        {title: "Status", dataIndex: "confirmation", key: "confirmation", render: (confirmation) => {
            if (confirmation === null || confirmation === "") {
                return <CloseCircleOutlined style={{color: "red"}}/>;
            } else if (confirmation === false) {
                return <CloseCircleOutlined style={{color: "red"}}/>;
            } else if (confirmation === true) {
                return <CheckCircleOutlined style={{color: "green"}}/>;
            }
            return null; // Handle other cases or null values
        },
    }, {
        title: "Image Profile", dataIndex: "imageProfile", render: (imageProfile) => {
            // Check if there are image profiles
            if (imageProfile && imageProfile.length > 0) {
                // You can render the image(s) here
                return (<div>
                        {imageProfile.map((profile) => (<Image src={profile.url} width={50}/>))}
                    </div>);
            }
            return "No image profile";
        },
    }, {
        title: "Image Source", dataIndex: "imageSrc", render: (imageSrc) => {
            // Check if there are image sources
            if (imageSrc && imageSrc.length > 0) {
                // You can render the image(s) here
                return (<div>
                        {imageSrc.map((src) => (<Image src={src.url} width={50}/>))}
                    </div>);
            }
            return "No image source";
        },
    },
    //     {
    //     title: "Action", key: "action", render: (text, record) => (<Button
    //             type="primary"
    //             onClick={() => updateConfirmation(record)}
    //         >
    //             Confirm
    //         </Button>),
    // },
        {
            title: "Action",
            key: "action",
            render: (text, record) => (
                <>
                    {record.confirmation !== true && (
                        <>
                            <Button type="primary" onClick={toggleConfirmationModal}>
                                Confirm
                            </Button>
                            <Modal
                                title="Confirm Action"
                                visible={isConfirmationModalVisible}
                                onOk={() => updateConfirmation(record)}
                                onCancel={toggleConfirmationModal}
                            >
                                Are you sure you want to confirm this action?
                            </Modal>
                        </>
                    )
                    }

                </>
            ),
        }
    ];

    const ADD_NEW_ENTITY = "Add new Guide";
    const DATATABLE_TITLE = "Guide List";
    const ENTITY_NAME = "admin";
    const CREATE_ENTITY = "Create admin";
    const UPDATE_ENTITY = "Update admin";

    const config = {
        entity, panelTitle, dataTableTitle, ENTITY_NAME, // CREATE_ENTITY,
        // ADD_NEW_ENTITY,
        // UPDATE_ENTITY,
        DATATABLE_TITLE, readColumns, dataTableColumns, searchConfig, entityDisplayLabels, showComponent
    };
    return (<ListingGuideModule
            createForm={<AdminForm/>}
            updateForm={<AdminForm isUpdateForm={true}/>}
            config={config}
        />);
}