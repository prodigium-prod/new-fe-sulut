import React from "react";

import CrudModule from "@/modules/CrudModule";
import PublicServicesForm from "@/forms/PublicServicesForm";

function PublicServices() {
    const entity = "publicservices";
    const searchConfig = {
        displayLabels: ["Name"],
        searchFields: "name",
        outputValue: "name",
    }

    const panelTitle = "Public Services Panel";
    const dataTableTitle = "Public Services List";
    const entityDisplayLabels = ["name"];

    const readColumns = [
        {
          title: "Name",
          dataIndex: "name",
        },
        {
          title: "Description",
          dataIndex: "about",
        },
        {
          title: "Address",
          dataIndex: "address",
        },
        {
          title: "Phone",
          dataIndex: "phone",
        },
        {
            title: "Website",
            dataIndex: "website",
        },
        {
            title: "Images",
            dataIndex: "imageSrc",
            render: (imageSrc) => (
                <>
                    {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                        imageSrc.map((image, index) => (
                            <img
                                key={index}
                                src={image.url}
                                alt={`Hotel Image ${index}`}
                                width="100"
                                style={{ marginRight: 10 }}
                            />
                        ))
                    ) : (
                        <span>No images available</span>
                    )}
                </>
            ),
        }

    ];
      const dataTableColumns = [
        {
          title: "Name",
          dataIndex: "name",
        },
        {
          title: "Description",
          dataIndex: "about",
        },
        {
          title: "Address",
          dataIndex: "address",
        },
        {
          title: "Phone",
          dataIndex: "phone",
        },
        {
            title: "Website",
            dataIndex: "website",
        },
          {
              title: "Images",
              dataIndex: "imageSrc",
              render: (imageSrc) => (
                  <>
                      {Array.isArray(imageSrc) && imageSrc.length > 0 ? (
                          imageSrc.map((image, index) => (
                              <img
                                  key={index}
                                  src={image.url}
                                  alt={`Hotel Image ${index}`}
                                  width="100"
                                  style={{ marginRight: 10 }}
                              />
                          ))
                      ) : (
                          <span>No images available</span>
                      )}
                  </>
              ),
          }
      ];

      const ADD_NEW_ENTITY = "Add New Public Services";
      const DATATABLE_TITLE = "Public Services List";
      const ENTITY_NAME = "publicservices";
      const CREATE_ENTITY = "Create Public Services";
      const UPDATE_ENTITY = "Update Public Services";
      const config = {
          entity,
          panelTitle,
          dataTableTitle,
          ENTITY_NAME,
          CREATE_ENTITY,
          ADD_NEW_ENTITY,
          UPDATE_ENTITY,
          DATATABLE_TITLE,
          readColumns,
          dataTableColumns,
          searchConfig,
          entityDisplayLabels,
      };
      return (
          <CrudModule
          createForm={<PublicServicesForm />}
          updateForm={<PublicServicesForm isUpdateForm={true} />}
          config={config}
          />
      );
}
export default PublicServices;