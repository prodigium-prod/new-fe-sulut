import React, { useState } from "react";
import { Button, Form, Input, Select, Upload, Collapse, message, TimePicker } from "antd";
import { InboxOutlined, UploadOutlined } from "@ant-design/icons";
import axios from 'axios';
import { useSelector } from "react-redux";



export default function PublicServicesForm({ isUpdateForm = false }) {

  const { Option, OptGroup } = Select;
  const { Dragger } = Upload;
  const { TextArea } = Input;
  const { Panel } = Collapse;

  const servicesPublicServices = [
    { value: '1', label: 'Toilet', icon: 'coffee' },
    { value: '2', label: 'ATM', icon: 'coffee' },
    { value: '3', label: 'Mini-mart', icon: 'wifi' },
    { value: '4', label: 'Groceries', icon: 'car' },
    { value: '5', label: 'Food & Drink', icon: 'car' },
    { value: '6', label: 'Health & Beauty', icon: 'car' },
    { value: '7', label: 'Home & Kitchen', icon: 'car' },
    { value: '8', label: 'Jewelry', icon: 'car' },
    { value: '9', label: 'Electronics', icon: 'car' },
    { value: '10', label: 'Pharmacy', icon: 'car' },
    { value: '11', label: 'Parking', icon: 'car' },
    { value: '12', label: 'Sport Court', icon: 'car' },
    { value: '13', label: 'Garden', icon: 'car' },
    { value: '14', label: 'IGD', icon: 'car' },
    { value: '15', label: 'UGD', icon: 'car' },
    { value: '16', label: 'Dental', icon: 'car' },
    { value: '17', label: 'Medical Check Up', icon: 'car' },
    { value: '18', label: 'BPJS', icon: 'car' },
    { value: '19', label: 'In-store shopping', icon: 'car' },
    { value: '20', label: 'In-store pick up', icon: 'car' },
    { value: '21', label: 'Delivery', icon: 'car' },
    { value: '22', label: 'Bus', icon: 'car' },
    { value: '23', label: 'Mini Bus', icon: 'car' },
    { value: '24', label: 'Train', icon: 'car' },
    { value: '25', label: 'Indonesian', icon: 'car' },
    { value: '26', label: 'Chinese', icon: 'car' },
    { value: '27', label: 'Minahasa', icon: 'car' },
    { value: '28', label: 'Gereja', icon: 'car' },
    { value: '29', label: 'Masjid', icon: 'car' },
    { value: '30', label: 'Mushola', icon: 'car' },
    { value: '31', label: 'Vihara', icon: 'car' },
    { value: '32', label: 'Pura', icon: 'car' },
    { value: '33', label: 'Klenteng', icon: 'car' },
    { value: '34', label: 'Car Wash', icon: 'car' },
    { value: '35', label: 'Car Repair', icon: 'car' }
  ];

  // const [form] = Form.useForm();
  const [isGeneralOpen, setIsGeneralOpen] = useState(false);
  const [isPublicOpen, setIsPublicOpen] = useState(false);
  const [isRoomOpen, setIsRoomOpen] = useState(false);
  const currentAdmin = useSelector((state) => state.auth.current);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [selectedSubcategory, setSelectedSubcategory] = useState(null);

  const handleGeneralClick = () => {
    setIsGeneralOpen(!isGeneralOpen);
  };

  const handlePublicClick = () => {
    setIsPublicOpen(!isPublicOpen);
  };

  const handleRoomClick = () => {
    setIsRoomOpen(!isRoomOpen);
  };

    const categories = [
        {
            label: 'Nature & Park',
            value: 1,
            options: ['National Park', 'Botanical Garden', 'Zoo', 'Beach', 'Hiking'],
        },
        {
            label: 'Shopping',
            value: 2,
            options: ['Minimart', 'Shopping Mall', 'Traditional Market', 'Boutique', 'Souvenir Shop'],
        },
        {
            label: 'Entertainment',
            value: 3,
            options: ['Theatre', 'Recreation', 'Massage', 'Bar', 'Lounge', 'Clubbing'],
        },
        {
            label: 'Transportation',
            value: 4,
            options: ['Rental', 'Airport', 'Public Transportation'],
        },
        {
            label: 'Public Services',
            value: 5,
            options: ['City Centre', 'Police', 'Hospital', 'Religion'],
        },
        {
            label: 'Historical & Culture',
            value: 6,
            options: ['Museum', 'Historical Landmark', 'Archaeological Site', 'Art Gallery'],
        },
    ];

    const handleCategoryChange = (value) => {
        setSelectedCategory(value);
        setSelectedSubcategory(null); // Reset the subcategory when changing the category
    };

    const handleSubcategoryChange = (value) => {
        setSelectedSubcategory(value);
    };

    const selectedCategoryObject = categories.find((category) =>
        category.value === selectedCategory
    );

    const subcategoryOptions = selectedCategoryObject ? selectedCategoryObject.options : [];


    return (
    <>
        <Form.Item
            label="Category"
            name="numcategory"
            rules={[
                {
                    required: true,
                    message: 'Please select a category',
                },
            ]}
        >
            <Select
                placeholder="Select a category"
                onChange={handleCategoryChange}
                value={selectedCategory}
            >
                {categories.map((category) => (
                    <Option key={category.value} value={category.value}>
                        {category.label}
                    </Option>
                ))}
            </Select>
        </Form.Item>
        <Form.Item
            label="Subcategory"
            name="category"
            rules={[
                {
                    required: true,
                    message: 'Please select a subcategory',
                },
            ]}
        >
            <Select
                placeholder="Select a subcategory"
                onChange={handleSubcategoryChange}
                value={selectedSubcategory}
                disabled={!selectedCategory}
            >
                {subcategoryOptions.map((option) => (
                    <Option key={option} value={option}>
                        {option}
                    </Option>
                ))}
            </Select>
        </Form.Item>
      <Form.Item
        label="Name"
        name="name"
        rules={[
          {
            required: true,
            message: "Please input name!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
          label="Description"
          name="about"
          rules={[
              {
                  required: true,
                  message: "Please provide an overview!",
              },
          ]}
      >
          <TextArea rows={4} />
      </Form.Item>
      <Form.Item
        label="Address"
        name="address"
        rules={[
          {
            required: true,
            message: "Please input an address!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Phone Number"
        name="phone"
        rules={[
          {
            required: true,
            message: "Please input phone number!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Website"
        name="website"
        rules={[
          {
            required: true,
            message: "Please input Website!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Facilities"
        name="facilities"
        rules={[
          {
            required: true,
            message: "Please select facilities!",
          },
        ]}
      >
        <Select mode="multiple">
          {servicesPublicServices.map((service) => (
            <Option key={service.value} value={service.value} icon={service.icon}>
              {service.label}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        label="Open Hour"
        name="open_hour"
        rules={[
            {
            required: true,
            message: "Please input open hour!",
            },
        ]}
        >
        <TimePicker format="HH:mm" />
      </Form.Item>

      <Form.Item
        label="Close Hour"
        name="close_hour"
        rules={[
            {
            required: true,
            message: "Please input close hour!",
            },
        ]}
        >
        <TimePicker format="HH:mm" />
      </Form.Item>
    </>
  );
}