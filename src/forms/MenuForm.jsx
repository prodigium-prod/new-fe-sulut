import React, { useState, useEffect } from "react";
import { Button, Form, Input, Select, Upload, Collapse, message, TimePicker } from "antd";
import { InboxOutlined, UploadOutlined } from "@ant-design/icons";
import axios from 'axios';
import { useSelector } from "react-redux";


const axiosInstance = axios.create({
    baseURL: 'http://b.visit-northsulawesi.com',
  });

export default function MenuForm({ isUpdateForm = false }) {
  const { Option } = Select;
  const { Dragger } = Upload;
  const { TextArea } = Input;
  const { Panel } = Collapse;
  // const [form] = Form.useForm();
  const [isGeneralOpen, setIsGeneralOpen] = useState(false);
  const [isPublicOpen, setIsPublicOpen] = useState(false);
  const [isRoomOpen, setIsRoomOpen] = useState(false);
  const [menuNames, setMenuNames] = useState([]);
  const currentAdmin = useSelector((state) => state.auth.current);
  const userId = "64d4f2712cd5f3b7e0eb9ae1";
  // console.log('Admin :', currentAdmin.id);

  useEffect(() => {
    // Fetch hotel names from API using userId
    axiosInstance.get(`/api/fnb/listing?userId=${userId}`)
      .then((response) => {
        console.log(response.data); // Log the fetched data
        setMenuNames(response.data.result);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [userId]);

  const handleMenuSelect = (value) => {
    const selectedMenu = menuNames.find((menu) => menu.name === value);
    if (selectedMenu) {
      setSelectedHotelId(selectedMenu.id);
    }
  };

  const handleGeneralClick = () => {
    setIsGeneralOpen(!isGeneralOpen);
  };

  const handlePublicClick = () => {
    setIsPublicOpen(!isPublicOpen);
  };

  const handleRoomClick = () => {
    setIsRoomOpen(!isRoomOpen);
  };
  
  return (
    <>
      <Form.Item
            label="FnB Name"
            name="fnbId"
            rules={[
            {
                required: true,
                message: "Please input the hotel title!",
            },
            ]}
      >
        <Select onChange={handleMenuSelect}>
          {menuNames.map((menu) => (
            <Select.Option key={menu.id} value={menu.id}>
              {menu.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        label="Menu Name"
        name="name"
        rules={[
          {
            required: true,
            message: "Please input name!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
          label="Description"
          name="description"
          rules={[
              {
                  required: true,
                  message: "Please provide an overview!",
              },
          ]}
      >
          <TextArea rows={4} />
      </Form.Item>
      <Form.Item
        label="Price"
        name="price"
        rules={[
          {
            required: true,
            message: "Please input the price!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Availability"
        name="availability"
        rules={[
          {
            required: true,
            message: "Please input availability of this menu!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Status"
        name="status"
        rules={[
          {
            required: true,
            message: "Please select a status of this menu!",
          },
        ]}
      >
        <Select>
          <Option value="Available">Available</Option>
          <Option value="Sold-Out">Sold-Out</Option>
          <Option value="Hotel">Coming Soon</Option>
        </Select>
      </Form.Item>
    </>
  );
}
