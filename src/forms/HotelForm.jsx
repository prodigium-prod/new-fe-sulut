import React, { useState } from "react";
import { Button, Form, Input, Select, Upload, Collapse, message } from "antd";
import { InboxOutlined, UploadOutlined } from "@ant-design/icons";
import axios from 'axios';
import { useSelector } from "react-redux";

export default function HotelForm({ isUpdateForm = false }) {
  const { Option } = Select;
  const { Dragger } = Upload;
  const { TextArea } = Input;
  const { Panel } = Collapse;

  const servicesGeneralOptions = [
    { value: '1', label: 'Free Welcome Drink', icon: 'coffee' },
    { value: '2', label: 'Free Snack', icon: 'coffee' },
    { value: '3', label: 'Free Wifi', icon: 'wifi' },
    { value: '4', label: 'Free Lounge', icon: 'car' },
    { value: '5', label: 'Free Parking', icon: 'car' }
  ];

  // const [form] = Form.useForm();
  const [isGeneralOpen, setIsGeneralOpen] = useState(false);
  const [isPublicOpen, setIsPublicOpen] = useState(false);
  const [isRoomOpen, setIsRoomOpen] = useState(false);
  const currentAdmin = useSelector((state) => state.auth.current);
  
  // console.log('Admin :', currentAdmin.id);

  const handleGeneralClick = () => {
    setIsGeneralOpen(!isGeneralOpen);
  };

  const handlePublicClick = () => {
    setIsPublicOpen(!isPublicOpen);
  };

  const handleRoomClick = () => {
    setIsRoomOpen(!isRoomOpen);
  };
  
  return (
    <>
      <Form.Item
        label="Title"
        name="title"
        rules={[
          {
            required: true,
            message: "Please input title!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Star"
        name="star"
        rules={[
          {
            required: true,
            message: "Please input star !",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Address"
        name="address"
        rules={[
          {
            required: true,
            message: "Please input an address!",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Phone Number"
        name="phone"
        rules={[
          {
            required: true,
            message: "Please input phone number!",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
          label="Description"
          name="description"
          rules={[
              {
                  required: true,
                  message: "Please provide an overview!",
              },
          ]}
      >
          <TextArea rows={4} />
      </Form.Item>
      <Form.Item
        label="Services"
        name="services"
        rules={[
          {
            required: true,
            message: "Please select service(s)!",
          },
        ]}
      >
        <Select mode="multiple">
          {servicesGeneralOptions.map((service) => (
            <Option key={service.value} value={service.value} icon={service.icon}>
              {service.label}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <div style={{ marginBottom: '40px' }}></div>
      <Button onClick={handleGeneralClick}>General Facility</Button>
      <Collapse activeKey={isGeneralOpen ? ['1'] : []}>
        <Panel header="General Facility" key="1">
          <Form.Item 
              label="AC" 
              name="general_ac"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Ballroom" 
              name="general_ballroom"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Swimming Pool" 
              name="general_pool"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Smooking Room" 
              name="general_smooking"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="24 Hour Receptionist" 
              name="general_receptionist"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Service Laundry" 
              name="general_laundry"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
        </Panel>
      </Collapse>

      <div style={{ marginBottom: '40px' }}></div>
      <Button onClick={handlePublicClick}>Public Facilities</Button>
      <Collapse activeKey={isPublicOpen ? ['1'] : []}>
        <Panel header="Public Facilities" key="1">
          <Form.Item 
              label="Parking" 
              name="pf_parking"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Coffee Shop" 
              name="pf_coffee"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Elevator" 
              name="pf_elevator"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="24h Room Services" 
              name="pf_roomservices"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Restaurant" 
              name="pf_restaurant"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Wifi" 
              name="pf_wifi"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Fitness Center" 
              name="pf_fitness"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="ATM" 
              name="pf_atm"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Minimarket" 
              name="pf_minimarket"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Beauty Salon" 
              name="pf_salon"
              rules={[
                {
                  required: true,
                  message: "",
                },
              ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
              label="Spa" 
              name="pf_spa"
              rules={[
                {
                  required: true,
                  message: "Please upload an image!",
                },
              ]}
          >
            <Input />
          </Form.Item>
        </Panel>
      </Collapse>
      <div style={{ marginBottom: '40px' }}></div>
    </>
  );
}
