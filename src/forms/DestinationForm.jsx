import React, { useState } from "react";
import { Button, Form, Input, Select, Upload, message } from "antd";
import { InboxOutlined, UploadOutlined } from "@ant-design/icons";
import axios from 'axios';


export default function DestinationForm({ isUpdateForm = false }) {
  const { Option } = Select;
  const { Dragger } = Upload;
  const [form] = Form.useForm();
  // const [uploadedImage, setUploadedImage] = useState(null);

  const uploadProps = {
    name: 'file',
    action: 'https://api.cloudinary.com/v1_1/dgbtjwctx/image/upload',
    data: {
      upload_preset: 'wwmb6npl',
    },
    onChange(info) {
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
          // Assuming the URL is on info.file.response.url
          form.setFieldsValue({
            image: info.file.response.url
          });
          message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
    },
  };
  
  return (
    <>
      <Form.Item
        label="Title"
        name="title"
        rules={[
          {
            required: true,
            message: "Please input title!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Description"
        name="description"
        rules={[
          {
            required: true,
            message: "Please input image of the place!",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Category"
        name="category"
        rules={[
          {
            required: true,
            message: "Please select a category!",
          },
        ]}
      >
        <Select>
          <Option value="Destination">Destination</Option>
          <Option value="Restaurant">Restaurant</Option>
          <Option value="Hotel">Hotel</Option>
          <Option value="Home Stay">Home Stay</Option>
          <Option value="National Park">National Park</Option>
          <Option value="Beach">Beach</Option>
          <Option value="Pool">Pool</Option>
        </Select>
      </Form.Item>

      <Form.Item
        name="price"
        label="price"
        rules={[
          {
            required: true,
            message: "Please input how many times the place was visited by visitors!",
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Room"
        name="roomCount"
        rules={[
          {
            required: true,
            message: "Please input room!",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Bathroom"
        name="bathroomCount"
        rules={[
          {
            required: true,
            message: "Please input Bathroom!",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Guest Room"
        name="guestCount"
        rules={[
          {
            required: true,
            message: "Please input guest room!",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Image"
        name="image"
        rules={[
          {
            required: true,
            message: "Please upload an image!",
          },
        ]}
      >
        <Upload {...uploadProps}>
          <Button icon={<UploadOutlined />}>
            Click or drag an image to upload
          </Button>
        </Upload>
      </Form.Item>
    </>
  );
}
