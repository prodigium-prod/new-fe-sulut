import React, { useState, useEffect } from "react";
import { Button, Form, Input, Select, Upload, Collapse, message } from "antd";
import { InboxOutlined, UploadOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import axios from 'axios';


const axiosInstance = axios.create({
  baseURL: 'http://b.visit-northsulawesi.com',
});

export default function HotelDetailForm({ isUpdateForm = false }) {
  const { Option } = Select;
  const { Dragger } = Upload;
  const { TextArea } = Input;
  const { Panel } = Collapse;
  // const [form] = Form.useForm();
  const [isGeneralOpen, setIsGeneralOpen] = useState(false);
  const [isPublicOpen, setIsPublicOpen] = useState(false);
  const [isRoomOpen, setIsRoomOpen] = useState(false);
  const currentAdmin = useSelector((state) => state.auth.current);
  const [hotelNames, setHotelNames] = useState([]);
  const [selectedHotelId, setSelectedHotelId] = useState("");


  const userId = currentAdmin.id;
  // console.log('UserID :', userId);

  useEffect(() => {
    // Fetch hotel names from API using userId
    axiosInstance.get(`/api/hotel/listing?userId=${userId}`)
      .then((response) => {
        console.log(response.data); // Log the fetched data
        setHotelNames(response.data.result);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [userId]);

  const handleHotelSelect = (value) => {
    const selectedHotel = hotelNames.find((hotel) => hotel.title === value);
    if (selectedHotel) {
      setSelectedHotelId(selectedHotel.id);
    }
  };
  
  const hotelConfirmation =  [
    { id: '1', name: 'Suite' },
    { id: '2', name: 'Standard Twin Room' },
    { id: '3', name: 'Deluxe' },
    { id: '4', name: 'Deluxe Room' },
    { id: '5', name: 'Deluxe King' },
    { id: '6', name: 'Classic' },
    { id: '7', name: 'Classic Room Only No Window' },
    { id: '8', name: 'Junior' },
    { id: '9', name: 'Junior Suite Room' },
    { id: '10', name: 'Superior' },
    { id: '11', name: 'Superior with Breakfast' },
    { id: '12', name: 'Executive' },
    { id: '13', name: 'Grand Deluxe' },
  ];

  const servicesOptions = [
    { value: '1', label: 'Free Wifi', icon: 'wifi' },
    { value: '2', label: 'Free Coffee', icon: 'coffee' },
    { value: '3', label: 'Free Bath', icon: 'bath' },
    { value: '4', label: 'Free Car', icon: 'car' },
    { value: '5', label: 'Free Paw', icon: 'paw' },
    { value: '6', label: 'Free Futbol', icon:'futbol' }
  ];
 
  const handleGeneralClick = () => {
    setIsGeneralOpen(!isGeneralOpen);
  };

  const handlePublicClick = () => {
    setIsPublicOpen(!isPublicOpen);
  };

  const handleRoomClick = () => {
    setIsRoomOpen(!isRoomOpen);
  };
  
  return (
    <>
      <Form.Item
        label="Hotel Name"
        name="hotelId"
        rules={[
          {
            required: true,
            message: "Please input the hotel title!",
          },
        ]}
      >
        <Select onChange={handleHotelSelect}>
          {hotelNames.map((hotel) => (
            <Select.Option key={hotel.id} value={hotel.id}>
              {hotel.title}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        label="Room Type"
        name="roomType"
        rules={[
            {
            required: true,
            message: "Please select room type!",
            },
        ]}
        style={{
            display: "inline-block",
            width: "calc(50%)",
            paddingRight: "5px",
        }}
        >
        <Select>
            {hotelConfirmation.map((hotel) => (
            <Option key={hotel.id} value={`${hotel.id}-${hotel.name}`}>
                {hotel.name}
            </Option>
            ))}
        </Select>
      </Form.Item>
      <Form.Item
        label="Price"
        name="price"
        rules={[
          {
            required: true,
            message: "Please input price !",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Available Room"
        name="available"
        rules={[
          {
            required: true,
            message: "Please input availability !",
          },
        ]}
        style={{
          display: "inline-block",
          width: "calc(50%)",
          paddingRight: "5px",
        }}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Services"
        name="services"
        rules={[
          {
            required: true,
            message: "Please select service(s)!",
          },
        ]}
      >
        <Select mode="multiple">
          {servicesOptions.map((service) => (
            <Option key={service.value} value={service.value} icon={service.icon}>
              {service.label}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <div style={{ marginBottom: '40px' }}></div>
      <Button onClick={handleRoomClick}>In-room Facilities</Button>
      <Collapse activeKey={isRoomOpen ? ['1'] : []}>
        <Panel header="In-room Facilities" key="1">
          <Form.Item 
                label="Bathrobe" 
                name="irf_bathrobe"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="Cable TV" 
                name="irf_cabletv"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="Desk" 
                name="irf_desk"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="Hairdryer" 
                name="irf_hairdryer"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="Mini Bar" 
                name="irf_minibar"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="Shower" 
                name="irf_shower"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="TV" 
                name="irf_tv"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="Smooking Room" 
                name="irf_smooking"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
          <Form.Item 
                label="Non-smooking Room" 
                name="irf_nonsmoking"
                rules={[
                  {
                    required: true,
                    message: "Please select service(s)!",
                  },
                ]}
          >
            <Input />
          </Form.Item>
        </Panel>
      </Collapse>
      <div style={{ marginBottom: '40px' }}></div>
    </>
  );
}
