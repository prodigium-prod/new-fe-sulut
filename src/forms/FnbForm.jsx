import React, {useState} from "react";
import {Collapse, Form, Input, Select, TimePicker, Upload} from "antd";
import {useSelector} from "react-redux";

export default function FnbForm({isUpdateForm = false}) {
    const {Option} = Select;
    const {Dragger} = Upload;
    const {TextArea} = Input;
    const {Panel} = Collapse;

    const categoryDining = [{value: 'Food', label: 'Food'}, {
        value: 'Culinary',
        label: 'Culinary'
    }, {value: 'Restaurant', label: 'Restaurant'}, {value: 'Cafe', label: 'Cafe'},];

    const paymentServices = [{value: '1', label: 'Tunai', icon: 'coffee'}, {
        value: '2',
        label: 'Visa',
        icon: 'coffee'
    }, {value: '3', label: 'Master', icon: 'wifi'}, {value: '4', label: 'Debit', icon: 'car'}, {
        value: '5',
        label: 'QRIS',
        icon: 'car'
    },];

    const facilitiesServices = [{value: '1', label: 'WiFi', icon: 'coffee'}, {
        value: '2', label: 'Delivery', icon: 'coffee'
    }, {value: '3', label: 'Smooking Area', icon: 'wifi'}, {
        value: '4', label: 'Non-Smooking Area', icon: 'car'
    }, {value: '5', label: '24 Hours', icon: 'car'}, {value: '6', label: '100% Halal', icon: 'car'}, {
        value: '7', label: 'VIP Room', icon: 'car'
    }, {value: '8', label: 'RSVP', icon: 'car'}, {value: '9', label: 'Parking Lot', icon: 'car'}, {
        value: '10', label: 'Alcohol', icon: 'car'
    }, {value: '11', label: 'Vegan', icon: 'car'},];

    // const [form] = Form.useForm();
    const [isGeneralOpen, setIsGeneralOpen] = useState(false);
    const [isPublicOpen, setIsPublicOpen] = useState(false);
    const [isRoomOpen, setIsRoomOpen] = useState(false);
    const currentAdmin = useSelector((state) => state.auth.current);

    // console.log('Admin :', currentAdmin.id);

    const handleGeneralClick = () => {
        setIsGeneralOpen(!isGeneralOpen);
    };

    const handlePublicClick = () => {
        setIsPublicOpen(!isPublicOpen);
    };

    const handleRoomClick = () => {
        setIsRoomOpen(!isRoomOpen);
    };

    return (<>
        <Form.Item
            label="Category"
            name="category"
            rules={[{
                required: true, message: "Please select category dining!",
            },]}
        >
            <Select>
                {categoryDining.map((service) => (<Option key={service.value} value={service.value}>
                    {service.label}
                </Option>))}
            </Select>
        </Form.Item>
        <Form.Item
            label="Name"
            name="name"
            rules={[{
                required: true, message: "Please input name!",
            },]}
        >
            <Input/>
        </Form.Item>
        <Form.Item
            label="Description"
            name="description"
            rules={[{
                required: true, message: "Please provide an overview!",
            },]}
        >
            <TextArea rows={4}/>
        </Form.Item>
        <Form.Item
            label="Address"
            name="address"
            rules={[{
                required: true, message: "Please input an address!",
            },]}
        >
            <Input/>
        </Form.Item>
        <Form.Item
            label="Phone Number"
            name="phone"
            rules={[{
                required: true, message: "Please input phone number!",
            },]}
        >
            <Input/>
        </Form.Item>
        <Form.Item
            label="Facilities"
            name="facilities"
            rules={[{
                required: true, message: "Please select facilities!",
            },]}
        >
            <Select mode="multiple">
                {facilitiesServices.map((service) => (
                    <Option key={service.value} value={service.value} icon={service.icon}>
                        {service.label}
                    </Option>))}
            </Select>
        </Form.Item>
        <Form.Item
            label="Payments"
            name="payment"
            rules={[{
                required: true, message: "Please select available payments!",
            },]}
        >
            <Select mode="multiple">
                {paymentServices.map((service) => (
                    <Option key={service.value} value={service.value} icon={service.icon}>
                        {service.label}
                    </Option>))}
            </Select>
        </Form.Item>
        <Form.Item
            label="Open Hour"
            name="open_hour"
            rules={[{
                required: true, message: "Please input open hour!",
            },]}
        >
            <TimePicker format="HH:mm"/>
        </Form.Item>

        <Form.Item
            label="Close Hour"
            name="close_hour"
            rules={[{
                required: true, message: "Please input close hour!",
            },]}
        >
            <TimePicker format="HH:mm"/>
        </Form.Item>
    </>);
}