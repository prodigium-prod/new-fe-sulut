import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {crud} from "@/redux/crud/actions";
import {useCrudContext} from "@/context/crud";
import {selectCreatedItem} from "@/redux/crud/selectors";
import {Button, Form, Input, message, Spin, Upload} from "antd";
import Loading from "@/components/Loading";
import MapPicker from 'react-google-map-picker'
import {UploadOutlined} from "@ant-design/icons";
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from 'react-places-autocomplete';
import axios from "axios";
import {LoadingOutlined} from "@ant-design/icons";


require('dotenv').config({path: '../../../variables.env'});

const DefaultLocation = {lat: 0.624693, lng: 123.974998};
const DefaultZoom = 10;

export default function CreateForm({config, formElements}) {
    let {entity, showComponent} = config;
    const dispatch = useDispatch();
    const {isLoading, isSuccess} = useSelector(selectCreatedItem);
    const {crudContextAction} = useCrudContext();
    const {panel, collapsedBox, readBox} = crudContextAction;
    const [defaultLocation, setDefaultLocation] = useState(DefaultLocation);
    const [location, setLocation] = useState(defaultLocation);
    const [zoom, setZoom] = useState(DefaultZoom);
    const [uploadedFile, setUploadedFile] = useState(null);
    const [form] = Form.useForm();
    const [imageUrls, setImageUrls] = useState([]); // Declare a new state variable
    const currentAdmin = useSelector((state) => state.auth.current);
    const [address, setAddress] = useState(""); // Add the address state
    const [googleMapsLoaded, setGoogleMapsLoaded] = useState(false); // Added state to track if Google Maps API is loaded

    const servicesPublicServices = [{id: '1', name: 'Toilet', icon: 'coffee'}, {
        id: '2', name: 'ATM', icon: 'coffee'
    }, {id: '3', name: 'Mini-mart', icon: 'wifi'}, {id: '4', name: 'Groceries', icon: 'car'}, {
        id: '5', name: 'Food & Drink', icon: 'car'
    }, {id: '6', name: 'Health & Beauty', icon: 'car'}, {id: '7', name: 'Home & Kitchen', icon: 'car'}, {
        id: '8', name: 'Jewelry', icon: 'car'
    }, {id: '9', name: 'Electronics', icon: 'car'}, {id: '10', name: 'Pharmacy', icon: 'car'}, {
        id: '11', name: 'Parking', icon: 'car'
    }, {id: '12', name: 'Sport Court', icon: 'car'}, {id: '13', name: 'Garden', icon: 'car'}, {
        id: '14', name: 'IGD', icon: 'car'
    }, {id: '15', name: 'UGD', icon: 'car'}, {id: '16', name: 'Dental', icon: 'car'}, {
        id: '17', name: 'Medical Check Up', icon: 'car'
    }, {id: '18', name: 'BPJS', icon: 'car'}, {id: '19', name: 'In-store shopping', icon: 'car'}, {
        id: '20', name: 'In-store pick up', icon: 'car'
    }, {id: '21', name: 'Delivery', icon: 'car'}, {id: '22', name: 'Bus', icon: 'car'}, {
        id: '23', name: 'Mini Bus', icon: 'car'
    }, {id: '24', name: 'Train', icon: 'car'}, {id: '25', name: 'Indonesian', icon: 'car'}, {
        id: '26', name: 'Chinese', icon: 'car'
    }, {id: '27', name: 'Minahasa', icon: 'car'}, {id: '28', name: 'Gereja', icon: 'car'}, {
        id: '29', name: 'Masjid', icon: 'car'
    }, {id: '30', name: 'Mushola', icon: 'car'}, {id: '31', name: 'Vihara', icon: 'car'}, {
        id: '32', name: 'Pura', icon: 'car'
    }, {id: '33', name: 'Klenteng', icon: 'car'}, {id: '34', name: 'Car Wash', icon: 'car'}, {
        id: '35', name: 'Car Repair', icon: 'car'
    }];

    const servicesGeneralOptions = [{id: '1', name: 'Free Welcome Drink', icon: 'coffee'}, {
        id: '2', name: 'Free Snack', icon: 'coffee'
    }, {id: '3', name: 'Free Wifi', icon: 'wifi'}, {id: '4', name: 'Free Lounge', icon: 'car'}, {
        id: '5', name: 'Free Parking', icon: 'car'
    }];

    const serviceOptions = [{id: '1', name: 'Free Wifi', icon: 'wifi'}, {
        id: '2', name: 'Free Coffee', icon: 'coffee'
    }, {id: '3', name: 'Free Bath', icon: 'bath'}, {id: '4', name: 'Free Car', icon: 'car'}, {
        id: '5', name: 'Free Paw', icon: 'paw'
    }, {id: '6', name: 'Free Futbol', icon: 'futbol'}];

    const hotelConfirmation = [{id: '1', name: 'Suite'}, {id: '2', name: 'Standard Twin Room'}, {
        id: '3', name: 'Deluxe'
    }, {id: '4', name: 'Deluxe Room'}, {id: '5', name: 'Deluxe King'}, {id: '6', name: 'Classic'}, {
        id: '7', name: 'Classic Room Only No Window'
    }, {id: '8', name: 'Junior'}, {id: '9', name: 'Junior Suite Room'}, {id: '10', name: 'Superior'}, {
        id: '11', name: 'Superior with Breakfast'
    }, {id: '12', name: 'Executive'}, {id: '13', name: 'Grand Deluxe'},];

    const paymentServices = [{id: '1', name: 'Tunai', icon: 'coffee'}, {
        id: '2', name: 'Visa', icon: 'coffee'
    }, {id: '3', name: 'Master', icon: 'wifi'}, {id: '4', name: 'Debit', icon: 'car'}, {
        id: '5', name: 'QRIS', icon: 'car'
    },];

    const facilitiesServices = [{id: '1', name: 'WiFi', icon: 'coffee'}, {
        id: '2', name: 'Delivery', icon: 'coffee'
    }, {id: '3', name: 'Smooking Area', icon: 'wifi'}, {id: '4', name: 'Non-Smooking Area', icon: 'car'}, {
        id: '5', name: '24 Hours', icon: 'car'
    }, {id: '6', name: '100% Halal', icon: 'car'}, {id: '7', name: 'VIP Room', icon: 'car'}, {
        id: '8', name: 'RSVP', icon: 'car'
    }, {id: '9', name: 'Parking Lot', icon: 'car'}, {id: '10', name: 'Alcohol', icon: 'car'}, {
        id: '11', name: 'Vegan', icon: 'car'
    },];

    const onSubmit = (fieldsValue) => {
        // console.log("TEST : ",fieldsValue);
        if (fieldsValue.services && fieldsValue.services.length > 0 && fieldsValue.roomType) {
            const services = fieldsValue.services.map((selectedServiceId, index) => {
                const service = serviceOptions.find((service) => service.id === selectedServiceId);
                return {
                    ...service, id: (index + 1).toString(),
                };
            });

            const images = fieldsValue.imageSrc.map((url) => ({url}));

            // Add null/undefined check before calling split() on fieldsValue.roomType
            const [hotelRoomId, hotelRoomName] = fieldsValue.roomType ? fieldsValue.roomType.split("-") : [null, null];

            const updatedFieldsValue = {
                ...fieldsValue, // userId: currentAdmin.id.toString(),
                userId: "64d4f2712cd5f3b7e0eb9ae1", services, roomType: {
                    id: hotelRoomId, name: hotelRoomName,
                }, hotelRoomId, hotelRoomName, imageSrc: {
                    create: images,
                },
            };

            console.log("Data RoomType : ", JSON.stringify(updatedFieldsValue));
            dispatch(crud.create(entity, updatedFieldsValue));
        } else {
            let openHour = '';
            let openMinute = '';
            let closeHour = '';
            let closeMinute = '';
            let facilities = '';
            let services = '';
            let payment = '';

            if (fieldsValue.open_hour && fieldsValue.close_hour) {
                const openDate = new Date(fieldsValue.open_hour);
                openHour = openDate.getHours().toString();
                openMinute = openDate.getMinutes().toString();

                const closeDate = new Date(fieldsValue.close_hour);
                closeHour = closeDate.getHours().toString();
                closeMinute = closeDate.getMinutes().toString();
            }


            // MENU
            if (fieldsValue.payment && fieldsValue.payment.length > 0) {
                facilities = fieldsValue.facilities ? fieldsValue.facilities.map((selectedServiceId, index) => {
                    const facility = facilitiesServices.find((service) => service.id === selectedServiceId);

                    if (facility) {
                        return {
                            ...facility, id: (index + 1).toString(),
                        };
                    }

                    return null; // Ignore the facility if not found in either options
                }).filter(Boolean) : [];


                payment = fieldsValue.payment ? fieldsValue.payment.map((selectedServiceId, index) => {
                    const service = paymentServices.find((service) => service.id === selectedServiceId);

                    if (service) {
                        return {
                            ...service, id: (index + 1).toString(),
                        };
                    }

                    return null; // Ignore the service if not found in either options
                }).filter(Boolean) : [];

                console.log("FACILITIES : ", facilities);
                console.log("PAYMENT    : ", payment);
                // MENU

            } else {
                facilities = fieldsValue.facilities ? fieldsValue.facilities.map((selectedServiceId, index) => {
                    const facility = servicesPublicServices.find((service) => service.id === selectedServiceId) || servicesGeneralOptions.find((service) => service.id === selectedServiceId);

                    if (facility) {
                        return {
                            ...facility, id: (index + 1).toString(),
                        };
                    }

                    return null; // Ignore the facility if not found in either options
                }).filter(Boolean) : [];

                services = fieldsValue.services ? fieldsValue.services.map((selectedServiceId, index) => {
                    const service = servicesPublicServices.find((service) => service.id === selectedServiceId) || servicesGeneralOptions.find((service) => service.id === selectedServiceId);

                    if (service) {
                        return {
                            ...service, id: (index + 1).toString(),
                        };
                    }

                    return null; // Ignore the service if not found in either options
                }).filter(Boolean) : [];

            }

            const images = fieldsValue.imageSrc.map((url) => ({url}));

            const updatedFieldsValue = {
                ...fieldsValue, // userId: currentAdmin.id.toString(),
                userId: "64d4f2712cd5f3b7e0eb9ae1", // createdBy: currentAdmin.name.toString(),
                payment: payment.length > 0 ? payment : undefined,
                facilities: facilities.length > 0 ? facilities : undefined,
                services: services.length > 0 ? services : undefined,
                imageSrc: {
                    create: images,
                },
                open_hour: openHour && openMinute ? `${openHour}:${openMinute}` : undefined,
                close_hour: closeHour && closeMinute ? `${closeHour}:${closeMinute}` : undefined,
                lat: fieldsValue.lat ? parseFloat(fieldsValue.lat) : undefined,
                lng: fieldsValue.lng ? parseFloat(fieldsValue.lng) : undefined,
            };
            console.log("Data : ", JSON.stringify(updatedFieldsValue));
            dispatch(crud.create(entity, updatedFieldsValue));
        }
    };

    useEffect(() => {
        // console.log("isSuccess isinya apa??: ",isSuccess);  //return false
        if (isSuccess) {
            readBox.open();
            collapsedBox.open();
            panel.open();
            form.resetFields();
            dispatch(crud.resetAction("create"));
            dispatch(crud.list(entity));
        }
    });

    // Set form field value every time imageUrls changes
    useEffect(() => {
        form.setFieldsValue({
            image: imageUrls, imageSrc: imageUrls
        });
    }, [imageUrls]);

    const searchOptions = {
        componentRestrictions: {country: 'id'},
    };
    function handleChangeLocation(lat, lng) {
        const apiKey = 'AIzaSyAuyS1LLibOZOGt-eliwsfzzTSYb3fVkmQ';
        const geocodingApiUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${apiKey}`;

        setLocation({lat: lat, lng: lng});

        axios
            .get(geocodingApiUrl)
            .then(response => {
                const address = response.data.results[0]?.formatted_address || "";
                setAddress(address);
                form.setFieldsValue({lat: lat, lng: lng, address: address});
            })
            .catch(error => {
                console.error('Error fetching address:', error);
            });
    }

    function handleChangeZoom(newZoom) {
        setZoom(newZoom);
    }

    useEffect(() => {
        // Load Google Maps API script
        const googleMapsScript = document.createElement('script');
        googleMapsScript.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyAuyS1LLibOZOGt-eliwsfzzTSYb3fVkmQ&libraries=places`;
        googleMapsScript.async = true;
        googleMapsScript.onload = handleGoogleMapsLoad;
        document.body.appendChild(googleMapsScript);

        // Cleanup script
        return () => {
            document.body.removeChild(googleMapsScript);
        };
    }, []);

    const handleGoogleMapsLoad = () => {
        setGoogleMapsLoaded(true);
    };


    const handleSelect = async (address) => {
        setAddress(address);
        try {
            if (typeof address === 'string') {
                const results = await geocodeByAddress(address);
                const latLng = await getLatLng(results[0]);
                setLocation(latLng);
                form.setFieldsValue({lat: latLng.lat, lng: latLng.lng, address: address});
            }
        } catch (error) {
            console.error('Error while geocoding address:', error);
        }
    };


    const uploadProps = {
        name: 'file', action: 'https://api.cloudinary.com/v1_1/dgbtjwctx/image/upload', data: {
            upload_preset: 'wwmb6npl',
        }, onChange(info) {
            const {status, response} = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done' && response) {
                setImageUrls(prevUrls => [...prevUrls, response.url]);
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    return (
        <Loading isLoading={isLoading}>
        {googleMapsLoaded ? ( // Render the form when Google Maps API is loaded
            <Form form={form} layout="vertical" onFinish={onSubmit}>
                {formElements}
                {showComponent ? <></> : <>
                    <Form.Item label="Search Places">
                        <PlacesAutocomplete value={address} onChange={setAddress} onSelect={handleSelect}
                                            searchOptions={searchOptions}>
                            {({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
                                <div>
                                    <Input
                                        {...getInputProps({placeholder: 'Search Places...'})}
                                        size="large"
                                    />
                                    <div
                                        style={{
                                            position: 'absolute',
                                            width: '100%',
                                            maxHeight: '200px',
                                            overflowY: 'auto',
                                            borderRadius: 3,
                                            backgroundColor: '#fff',
                                            zIndex: 10,
                                        }}
                                    >
                                        {loading && (
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    padding: '10px',
                                                }}
                                            >
                                                <Spin indicator={<LoadingOutlined style={{fontSize: 24}} spin/>}/>
                                            </div>
                                        )}
                                        {suggestions.map((suggestion, index) => (
                                            <div
                                                {...getSuggestionItemProps(suggestion, {key: index})}
                                                style={{padding: '10px', cursor: 'pointer'}}
                                            >
                                                {suggestion.description}
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            )}
                        </PlacesAutocomplete>
                    </Form.Item>
                    <Form.Item label="Choose Location" style={{marginBottom: 20}}>
                        <MapPicker defaultLocation={location}
                                   zoom={zoom}
                                   mapTypeId="roadmap"
                                   style={{height: '300px'}}
                                   onChangeLocation={handleChangeLocation}
                                   onChangeZoom={handleChangeZoom}
                                   apiKey="AIzaSyAuyS1LLibOZOGt-eliwsfzzTSYb3fVkmQ"
                        />
                    </Form.Item>
                    <Form.Item
                        name="lat" label="Latitude"
                        hidden={true}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        name="lng" label="Longitude"
                        hidden={true}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        label="Image"
                        name="imageSrc"
                        rules={[{
                            required: true, message: "Please upload an image!",
                        },]}
                    >
                        <Upload {...uploadProps} multiple={true}>
                            <Button icon={<UploadOutlined/>}>
                                Click or drag an image to upload
                            </Button>
                        </Upload>
                    </Form.Item>
                </>}
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>) : (<div>Loading Google Maps...</div>)}
    </Loading>);
}